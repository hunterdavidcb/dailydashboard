﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewAllPanel : MonoBehaviour
{
	public GameObject graphWindowPrefab;
	public Transform contentHolder;


	Dictionary<string, GameObject> graphDictionary = new Dictionary<string, GameObject>();

	public void SpawnGraphs(Dictionary<string,DashboardData> data)
	{
		Clear();

		foreach (var item in data.Values)
		{
			GameObject graph = Instantiate(graphWindowPrefab, contentHolder);
			List<int> success = new List<int>();
			List<DateTime> dates = new List<DateTime>();
			List<string> comments = new List<string>();
			foreach (var time in item.History)
			{
				dates.Add(time.Key);
				success.Add(time.Value.level);
				comments.Add(time.Value.comment);
			}

			graph.GetComponent<Window_Graph>().SetData(success, dates, comments);

			graphDictionary.Add(item.ID, graph);
		}
	}

	void Clear()
	{
		foreach (var item in graphDictionary.Values)
		{
			Destroy(item);
		}

		graphDictionary.Clear();
	}

}
