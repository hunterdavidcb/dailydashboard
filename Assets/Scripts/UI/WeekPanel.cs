﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class WeekPanel : MonoBehaviour
{
	public Text weekInfo;
	public GameObject mondayHolder;
	public GameObject tuesdayHolder;
	public GameObject wednesdayHolder;
	public GameObject thursdayHolder;
	public GameObject fridayHolder;
	public GameObject saturdayHolder;
	public GameObject sundayHolder;

	public Button previousWeekButton, nextWeekButton;

	public GameObject habitPrefab;

	Dictionary<string, List<GameObject>> habitObjects = new Dictionary<string, List<GameObject>>();
	Dictionary<GameObject, List<GameObject>> weekdayHabits = new Dictionary<GameObject, List<GameObject>>();

	DateTime weekStart;

	public delegate void DayClickHandler(DateTime dateTime);
	public event DayClickHandler DayClicked;

	public delegate void HabitClickHandler(string id);
	public event HabitClickHandler HabitClicked;

	private void Awake()
	{
		//weekStart
		weekdayHabits.Add(mondayHolder, new List<GameObject>());
		weekdayHabits.Add(tuesdayHolder, new List<GameObject>());
		weekdayHabits.Add(wednesdayHolder, new List<GameObject>());
		weekdayHabits.Add(thursdayHolder, new List<GameObject>());
		weekdayHabits.Add(fridayHolder, new List<GameObject>());
		weekdayHabits.Add(saturdayHolder, new List<GameObject>());
		weekdayHabits.Add(sundayHolder, new List<GameObject>());
	}

	//public void SpawnHabits(Dictionary<Habit, List<IPeriod>> habits)
	//{
	//	ClearList();
	//	foreach (var habit in habits)
	//	{
	//		foreach (var period in habit.Value)
	//		{
	//			switch (period.StartTime.DayOfWeek)
	//			{
	//				case DayOfWeek.Friday:
	//					if (weekdayHabits[fridayHolder].Count < 6)
	//					{
	//						//it is safe to add to the list

	//						//we should check to see about the time order 
	//						//how should we do this???
	//						GameObject habitObject = Instantiate(habitPrefab, fridayHolder.transform);
							
	//						AddHabitObject(fridayHolder, habitObject, habit.Key);

	//					}
	//					else
	//					{
	//						//add a day preview expansion button (+)
	//					}

	//					fridayHolder.GetComponentInParent<Button>().onClick.RemoveAllListeners();
	//					fridayHolder.GetComponentInParent<Button>().onClick.AddListener(delegate { OnDayClicked(period.StartTime.Value); });
	//					break;
	//				case DayOfWeek.Monday:
	//					if (weekdayHabits[mondayHolder].Count < 6)
	//					{
	//						//it is safe to add to the list

	//						//we should check to see about the time order 
	//						//how should we do this???
	//						GameObject habitObject = Instantiate(habitPrefab, mondayHolder.transform);
							
	//						AddHabitObject(mondayHolder, habitObject, habit.Key);
	//					}
	//					else
	//					{
	//						//add a day preview expansion button (+)
	//					}

	//					mondayHolder.GetComponentInParent<Button>().onClick.RemoveAllListeners();
	//					mondayHolder.GetComponentInParent<Button>().onClick.AddListener(delegate { OnDayClicked(period.StartTime.Value); });
	//					break;
	//				case DayOfWeek.Saturday:
	//					if (weekdayHabits[saturdayHolder].Count < 6)
	//					{
	//						//it is safe to add to the list

	//						//we should check to see about the time order 
	//						//how should we do this???
	//						GameObject habitObject = Instantiate(habitPrefab, saturdayHolder.transform);
							
	//						AddHabitObject(saturdayHolder, habitObject, habit.Key);
	//					}
	//					else
	//					{
	//						//add a day preview expansion button (+)
	//					}
	//					saturdayHolder.GetComponentInParent<Button>().onClick.RemoveAllListeners();
	//					saturdayHolder.GetComponentInParent<Button>().onClick.AddListener(delegate { OnDayClicked(period.StartTime.Value); });
						
	//					break;
	//				case DayOfWeek.Sunday:
	//					if (weekdayHabits[sundayHolder].Count < 6)
	//					{
	//						//it is safe to add to the list
	//						//Debug.Log("adding sunday");
	//						//we should check to see about the time order 
	//						//how should we do this???
	//						GameObject habitObject = Instantiate(habitPrefab, sundayHolder.transform);
							
	//						AddHabitObject(sundayHolder, habitObject, habit.Key);
	//					}
	//					else
	//					{
	//						//add a day preview expansion button (+)
	//					}
	//					sundayHolder.GetComponentInParent<Button>().onClick.RemoveAllListeners();
	//					sundayHolder.GetComponentInParent<Button>().onClick.AddListener(delegate { OnDayClicked(period.StartTime.Value); });
	//					break;
	//				case DayOfWeek.Thursday:
	//					if (weekdayHabits[thursdayHolder].Count < 6)
	//					{
	//						//it is safe to add to the list

	//						//we should check to see about the time order 
	//						//how should we do this???
	//						GameObject habitObject = Instantiate(habitPrefab, thursdayHolder.transform);
							
	//						AddHabitObject(thursdayHolder, habitObject, habit.Key);
	//					}
	//					else
	//					{
	//						//add a day preview expansion button (+)
	//					}
	//					thursdayHolder.GetComponentInParent<Button>().onClick.RemoveAllListeners();
	//					thursdayHolder.GetComponentInParent<Button>().onClick.AddListener(delegate { OnDayClicked(period.StartTime.Value); });
	//					break;
	//				case DayOfWeek.Tuesday:
	//					if (weekdayHabits[tuesdayHolder].Count < 6)
	//					{
	//						//it is safe to add to the list

	//						//we should check to see about the time order 
	//						//how should we do this???
	//						GameObject habitObject = Instantiate(habitPrefab, tuesdayHolder.transform);
							
	//						AddHabitObject(tuesdayHolder, habitObject, habit.Key);
	//					}
	//					else
	//					{
	//						//add a day preview expansion button (+)
	//					}
	//					tuesdayHolder.GetComponentInParent<Button>().onClick.RemoveAllListeners();
	//					tuesdayHolder.GetComponentInParent<Button>().onClick.AddListener(delegate { OnDayClicked(period.StartTime.Value); });
	//					break;
	//				case DayOfWeek.Wednesday:
	//					if (weekdayHabits[wednesdayHolder].Count < 6)
	//					{
	//						//it is safe to add to the list

	//						//we should check to see about the time order 
	//						//how should we do this???
	//						GameObject habitObject = Instantiate(habitPrefab, wednesdayHolder.transform);
							
	//						AddHabitObject(wednesdayHolder, habitObject, habit.Key);
	//					}
	//					else
	//					{
	//						//add a day preview expansion button (+)
	//					}
	//					wednesdayHolder.GetComponentInParent<Button>().onClick.RemoveAllListeners();
	//					wednesdayHolder.GetComponentInParent<Button>().onClick.AddListener(delegate { OnDayClicked(period.StartTime.Value); });
	//					break;
	//				default:
	//					break;
	//			}
	//		}
			
	//	}
	//}

	public void ClearList()
	{
		List<string> keys = habitObjects.Keys.ToList();

		for (int i = 0; i < keys.Count; i++)
		{
			//avoid out of range errors
			for (int x = habitObjects[keys[i]].Count - 1; x > -1; x--)
			{
				Destroy(habitObjects[keys[i]][x]);
				//Debug.Log("destroying");
			}
		}

		keys.Clear();
		habitObjects.Clear();
		foreach (var day in weekdayHabits)
		{
			day.Value.Clear();
		}
	}

	//void AddHabitObject(GameObject holder, GameObject habit, Habit hab)
	//{
	//	weekdayHabits[holder].Add(habit);
	//	if (!habitObjects.ContainsKey(hab.HabitID))
	//	{
	//		habitObjects.Add(hab.HabitID, new List<GameObject>() { habit });
	//	}
	//	else
	//	{
	//		habitObjects[hab.HabitID].Add(habit);
	//	}
	//	habit.GetComponent<Image>().color = hab.HabitColor;
	//	habit.GetComponent<Button>().onClick.AddListener(delegate { OnHabitClicked(hab.HabitID); });
	//}

	protected void OnHabitClicked(string id)
	{
		if (HabitClicked != null)
		{
			Debug.Log("calling");
			HabitClicked(id);
		}
	}

	protected void OnDayClicked(DateTime dateTime)
	{
		if (DayClicked != null)
		{
			DayClicked(dateTime);
		}
	}

	public static DateTime FirstDateOfWeek(string week)
	{
		char[] splitter = new char[] { '-'};
		int year = int.Parse(week.Split(splitter)[1]);
		int weekNumber = int.Parse(week.Split(splitter)[0].Remove(0));

		DateTime jan1 = new DateTime(year, 1, 1);
		int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

		DateTime firstThursday = jan1.AddDays(daysOffset);
		var cal = CultureInfo.CurrentCulture.Calendar;

		int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

		var weekNum = weekNumber;
		if (firstWeek == 1)
		{
			weekNum -= 1;
		}

		var result = firstThursday.AddDays(weekNum * 7);


		return result.AddDays(-3);
	}

	public static DateTime FirstDateOfWeek(int weekNumber, int year)
	{
		
		DateTime jan1 = new DateTime(year, 1, 1);
		int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

		DateTime firstThursday = jan1.AddDays(daysOffset);
		var cal = CultureInfo.CurrentCulture.Calendar;

		int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

		var weekNum = weekNumber;
		if (firstWeek == 1)
		{
			weekNum -= 1;
		}


		var result = firstThursday.AddDays(weekNum * 7);


		return result.AddDays(-3);
	}

	public static int WeekNumber(DateTime date)
	{
		var cal = CultureInfo.CurrentCulture.Calendar;

		return cal.GetWeekOfYear(date, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

	}
}
