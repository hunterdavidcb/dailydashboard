﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DashboardEditPanel : MonoBehaviour
{
	public Text nameText, description, levelOne, levelTwo, levelThree, levelFour, levelFive;

	public Button editName, editDescription, editLevelOne, editLevelTwo, editLevelThree, editLevelFour, editLevelFive, acceptButton, removeButton;
	public InputField editorInput;
	public Toggle levelView;

	public GameObject editorPanel;
	public GameObject confirmationPanel;
	public GameObject levelHolder;
	public Button confirmRemoveButton, cancelRemoveButton;

	public Window_Graph graph;

	public delegate void DashboardEditHandler(string id, string p, string v);
	public event DashboardEditHandler DashboardEdited;
	public event DashboardEditHandler DashboardRemoved;

	string ddID = null;

	public void SetValues(DashboardData dd)
	{
		//set the data from dd
		nameText.text = dd.Name;
		description.text = dd.Description;
		levelOne.text = dd.LevelOnePerformanceCriteria;
		levelTwo.text = dd.LevelTwoPerformanceCriteria;
		levelThree.text = dd.LevelThreePerformanceCriteria;
		levelFour.text = dd.LevelFourPerformanceCriteria;
		levelFive.text = dd.LevelFivePerformanceCriteria;

		ddID = dd.ID;
		//set up the listeners
		removeButton.onClick.AddListener(OnDashboardRemovePressed);
	}

	private void Awake()
	{
		editName.onClick.AddListener(() => OnEditCallback("Name"));
		editDescription.onClick.AddListener(() => OnEditCallback("Description"));
		editLevelOne.onClick.AddListener(() => OnEditCallback("Level One"));
		editLevelTwo.onClick.AddListener(() => OnEditCallback("Level Two"));
		editLevelThree.onClick.AddListener(() => OnEditCallback("Level Three"));
		editLevelFour.onClick.AddListener(() => OnEditCallback("Level Four"));
		editLevelFive.onClick.AddListener(() => OnEditCallback("Level Five"));

		levelHolder.SetActive(false);
		levelView.isOn = false;
		levelView.onValueChanged.AddListener(OnViewChanged);
	}

	private void OnViewChanged(bool b)
	{
		levelHolder.SetActive(b);
	}

	private void OnEditCallback(string v)
	{
		switch (v)
		{
			case "Name":
				break;
			case "Description":
				break;
			case "Level One":
				break;
			case "Level Two":
				break;
			case "Level Three":
				break;
			case "Level Four":
				break;
			case "Level Five":
				break;
			default:
				break;
		}

		acceptButton.onClick.RemoveAllListeners();
		acceptButton.onClick.AddListener(() => OnEditFinishedCallback(v));
		editorPanel.SetActive(true);
		((Text)editorInput.placeholder).text = "Enter your new " + v;
		//throw new NotImplementedException();
	}

	private void OnEditFinishedCallback(string editType)
	{
		switch (editType)
		{
			case "Name":
				nameText.text = editorInput.text;
				break;
			case "Description":
				description.text = editorInput.text;
				break;
			case "Level One":
				levelOne.text = editorInput.text;
				break;
			case "Level Two":
				levelTwo.text = editorInput.text;
				break;
			case "Level Three":
				levelThree.text = editorInput.text;
				break;
			case "Level Four":
				levelFour.text = editorInput.text;
				break;
			case "Level Five":
				levelFive.text = editorInput.text;
				break;
			default:
				break;
		}

		editorPanel.SetActive(false);

		//throw new NotImplementedException();
		if (DashboardEdited != null)
		{
			DashboardEdited(ddID,editType, editorInput.text);
		}
		
	}

	void OnDashboardRemovePressed()
	{
		//open confirmation panel
		confirmationPanel.SetActive(true);
		confirmationPanel.GetComponentInChildren<Text>().text =
			confirmationPanel.GetComponentInChildren<Text>().text.Replace("***", nameText.text);
		confirmRemoveButton.onClick.RemoveAllListeners();
		confirmRemoveButton.onClick.AddListener(() => OnDashboardRemoved(ddID));
		cancelRemoveButton.onClick.AddListener(OnRemoveCanceled);
	}

	private void OnRemoveCanceled()
	{
		confirmationPanel.SetActive(false);
	}

	void OnDashboardRemoved(string id)
	{
		if (DashboardRemoved != null)
		{
			DashboardRemoved(id, "", "");
		}
	}

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
