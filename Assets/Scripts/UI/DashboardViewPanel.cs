﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DashboardViewPanel : MonoBehaviour
{
	public Transform dashboardParent;
	public GameObject dashboardPref;

	public delegate void DashboardClickHandler(string id);
	public event DashboardClickHandler DashboardClicked;

	Dictionary<string, GameObject> dashboardObjects = new Dictionary<string, GameObject>();


	public void SpawnDashboards(Dictionary<string,DashboardData> dds)
	{
		Clear();

		foreach (var item in dds.Values)
		{
			GameObject go = Instantiate(dashboardPref, dashboardParent);
			go.GetComponentInChildren<Text>().text = item.Name;
			go.GetComponentInChildren<Button>().onClick.AddListener(() => DashboardClicked(item.ID));

			//don't forget to add them to the dictionary!!
			dashboardObjects.Add(item.ID, go);
		}
	}

	public void Clear()
	{
		foreach (var item in dashboardObjects.Values)
		{
			Destroy(item);
		}

		dashboardObjects.Clear();
	}

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
