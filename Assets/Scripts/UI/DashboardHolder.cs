﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DashboardHolder : MonoBehaviour
{
	public RectTransform pointer;
	public GameObject circle;
	public GameObject tooltip; //use to display the criteria for each stage/level

	public int stage;
	public string comment;

	public InputField commentField;

	DashboardData dd;
    // Start is called before the first frame update
    void Start()
    {
		Input.simulateMouseWithTouches = true;

    }

	private void Awake()
	{
		circle.GetComponent<CircleClick>().Clicked += OnClicked;
		circle.GetComponent<CircleClick>().PointerOver += OnPointerOver;
		circle.GetComponent<CircleClick>().PointerNotOver += OnPointerNotOver;
		tooltip.SetActive(false);
	}

	private void OnPointerNotOver(Vector2 pos)
	{
		tooltip.SetActive(false);
	}

	public void SetDD(DashboardData d)
	{
		dd = d;
	}

	private void OnPointerOver(Vector2 pos)
	{
		if (!tooltip.activeSelf)
		{
			tooltip.SetActive(true);
		}

		int localStage = -1;
		//Debug.Log(pos);
		Vector2 newPos;
		RectTransformUtility.ScreenPointToLocalPointInRectangle(GetComponent<RectTransform>(),
			pos, Camera.main, out newPos);
		newPos += new Vector2(0, 40);

		tooltip.GetComponent<RectTransform>().anchoredPosition = newPos;
		localStage = GetStage(AngleBetween(newPos - new Vector2(0, 40), pointer.anchoredPosition));
		if (dd != null)
		{
			switch (localStage)
			{
				case 1:
					tooltip.GetComponentInChildren<Text>().text = dd.LevelOnePerformanceCriteria;
					break;
				case 2:
					tooltip.GetComponentInChildren<Text>().text = dd.LevelTwoPerformanceCriteria;
					break;
				case 3:
					tooltip.GetComponentInChildren<Text>().text = dd.LevelThreePerformanceCriteria;
					break;
				case 4:
					tooltip.GetComponentInChildren<Text>().text = dd.LevelFourPerformanceCriteria;
					break;
				case 5:
					tooltip.GetComponentInChildren<Text>().text = dd.LevelFivePerformanceCriteria;
					break;
			}
		}
		
		//Debug.Log(localStage);
	}

	private void OnClicked(Vector2 pos)
	{
		RectTransformUtility.ScreenPointToLocalPointInRectangle(GetComponent<RectTransform>(), pos, Camera.main, out pos);
		//pos now is relative from the anchor
		Debug.Log(pos);
		float angle = AngleBetween(pos, pointer.anchoredPosition);

		pointer.localEulerAngles = new Vector3(0f, 0f, angle);
		stage = GetStage(angle);
		Debug.Log(stage);
	}

	float AngleBetween(Vector2 a, Vector2 b)
	{
		float y = a.y - b.y;
		float x = a.x - b.x;
		float angle = Mathf.Atan2(y, x) * Mathf.Rad2Deg - 90;
		//Debug.Log(pointer.anchoredPosition);
		//Debug.Log(pointer.position);
		angle -= 85;
		angle = Mathf.Clamp(angle, -170, 0);
		//Debug.Log(angle);
		float angleCopy = angle;
		int stage = GetStage(angleCopy);
		float offset = stage > 2 ? -10 : 0;
		angle = -(stage - 1) * 40 + offset;
		return angle;
	}

	int GetStage(float angle)
	{
		return Mathf.CeilToInt(Mathf.Abs((angle - 15) / 40));
	}

}
