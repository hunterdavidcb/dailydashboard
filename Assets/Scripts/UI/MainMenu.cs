﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using Unity.Notifications.Android;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
	public Button createButton;
	public Button editButton;
	public Button checkButton;
	public Button showAllButton;

	public GameObject createPanel;
	public GameObject checkPanel;
	public GameObject dashboardEditPanel;
	public GameObject dashboardViewPanel;
	public GameObject dashboardViewAllPanel;
	public Transform checkContent;
	

	public GameObject dashboardPrefab;

	Dictionary<string, DashboardData> data;
	Dictionary<DateTime, Dictionary<string, GameObject>> currentItems = new Dictionary<DateTime, Dictionary<string, GameObject>>();

	Dictionary<int, string> notificationDashboardDictionary = new Dictionary<int, string>();

	private Animator mainButtonHolderAnim;

	GameObject currentPanel = null;

    // Start is called before the first frame update
    void Start()
    {
		mainButtonHolderAnim = GetComponentInChildren<Animator>();
		data = LoadAndSave.Deserialize();

		DeserializeNotifications();

		// setup notifications
		var androidNotificationChannel = new AndroidNotificationChannel()
		{
			Id = "Habit_Hunter_Channel",
			Name = "Habit Hunter",
			Importance = Importance.High,
			Description = "Notifications for habits"
		};

		if (!AndroidNotificationCenter.GetNotificationChannels().Contains(androidNotificationChannel))
		{
			Debug.Log("registering");
			AndroidNotificationCenter.RegisterNotificationChannel(androidNotificationChannel);
		}


		var notification = new AndroidNotification()
		{
			Title = "Habit Hunter",
			Text = "Remember to check your habits",
			FireTime = DateTime.Today.AddHours(23),
			SmallIcon = "habit",
			RepeatInterval = new TimeSpan(1, 0, 0, 0)
		};

		//AndroidNotificationCenter.UpdateScheduledNotification(1, notification, androidNotificationChannel.Id);

		//if (AndroidNotificationCenter.CheckScheduledNotificationStatus(1) == NotificationStatus.Unavailable ||
		//	AndroidNotificationCenter.CheckScheduledNotificationStatus(1) == NotificationStatus.Unknown)
		//{

		AndroidNotificationCenter.SendNotificationWithExplicitID(notification, androidNotificationChannel.Id, 1);
		if (!notificationDashboardDictionary.ContainsKey(1))
		{
			notificationDashboardDictionary.Add(1, "Habit Hunter");
			SerializeNotifications();
		}

		var hollow = new AndroidNotification()
		{
			Title = "Habit Hunter",
			Text = "Processing habits",
			FireTime = DateTime.Today.AddDays(1).AddHours(1),
			SmallIcon = "habit",
			RepeatInterval = new TimeSpan(1, 0, 0, 0)
		};

		AndroidNotificationCenter.SendNotificationWithExplicitID(hollow, androidNotificationChannel.Id, 2);
		AndroidNotificationCenter.OnNotificationReceived += ProcessHabits;
		if (!notificationDashboardDictionary.ContainsKey(2))
		{
			notificationDashboardDictionary.Add(2, "Processing habits");
			SerializeNotifications();
		}

		//}

		ScheduleNotifications(androidNotificationChannel);

		createButton.gameObject.SetActive(data.Count < 5);
		showAllButton.gameObject.SetActive(data.Count > 0);
		
		editButton.gameObject.SetActive(data.Count > 0);
		

		createPanel.SetActive(false);
		checkPanel.SetActive(false);

		//set up listeners
		createButton.onClick.AddListener(OnCreateButtonPressed);
		editButton.onClick.AddListener(OnEditButtonPressed);
		checkButton.onClick.AddListener(OnCheckButtonPressed);
		showAllButton.onClick.AddListener(OnShowAllPressed);

		//createPanel.GetComponent<CreatePanel>().acceptButton.onClick.AddListener(OnAcceptButtonPressed);
		createPanel.GetComponent<CreatePanel>().DashboardCreated += OnDashboardCreated;
		createPanel.GetComponent<CreatePanel>().cancelButton.onClick.AddListener(OnCancelButtonPressed);

		dashboardViewPanel.GetComponent<DashboardViewPanel>().DashboardClicked += OnDashboardClicked;
		dashboardEditPanel.GetComponent<DashboardEditPanel>().DashboardEdited += OnDashboardEdited;
		dashboardEditPanel.GetComponent<DashboardEditPanel>().DashboardRemoved += OnDashboardRemoved;


		if (data.Count == 0)
		{
			editButton.gameObject.SetActive(false);
			checkButton.gameObject.SetActive(false);
		}
		else if (data.Count == 5)
		{
			createButton.gameObject.SetActive(false);
		}

		

		//for debugging
		foreach (var item in data)
		{
			Debug.Log(item.Value.Name);
			Debug.Log(item.Value.Description);
			if (item.Value.Start == null || item.Value.Start == DateTime.MinValue)
			{
				Debug.Log("it's null");
				item.Value.ChangeStartDate(DateTime.Today);
			}
			//Debug.Log(item)

			foreach (var time in item.Value.History)
			{
				Debug.Log(time.Key + " " + time.Value.comment + " " + time.Value.level);
			}
		}
		LoadAndSave.Serialize(data);
	}

	private void ScheduleNotifications(AndroidNotificationChannel channel)
	{
		int id = 3;
		foreach (var dashboard in data.Values)
		{
			DateTime next = DateTime.Today.AddHours(23);

			var notification = new AndroidNotification()
			{
				Title = "Daily Dashboard",
				Text = "Did you " + dashboard.Name + " today?",
				//FireTime = DateTime.Now.AddSeconds(id),
				FireTime = next,
				LargeIcon = "dashboard",

				//RepeatInterval = new TimeSpan(1, 0, 0, 0)
			};

			AndroidNotificationCenter.SendNotificationWithExplicitID(notification, channel.Id, id);

			AndroidNotificationCenter.OnNotificationReceived += OnNotificationSent;
			if (!notificationDashboardDictionary.ContainsKey(id))
			{
				notificationDashboardDictionary.Add(id, dashboard.ID);
				SerializeNotifications();
			}

			id++;

		}
	}

	protected void ProcessHabits(AndroidNotificationIntentData data)
	{
		if (data.Id == 2)
		{
			//TODO: fix this

			//Dictionary<Habit, List<IPeriod>> habs = GetHabitsBetween(DateTime.Today.AddDays(-1),
			//	DateTime.Today.AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(58));
			//foreach (var habit in habs.Keys)
			//{
			//	foreach (var time in habs[habit])
			//	{
			//		//the user did not check this particular habit today, so fail them
			//		if (!habit.CompletedInstances.ContainsKey(time.StartTime.Value))
			//		{
			//			habit.CompletedInstances.Add(time.StartTime.Value, false);
			//		}
			//	}
			//}
			////make sure to save the habits now that we've updated them
			//SerializeHabits();
		}
	}

	protected void OnNotificationSent(AndroidNotificationIntentData data)
	{
		//data.Id
		//data.Channel
		//data.Notification.Text
		//data.Notification.Title
		//data.Notification.FireTime
		//Debug.Log("here");
	}

	private void SerializeNotifications()
	{
		FileStream fileStream = new FileStream(Application.persistentDataPath + "/Notifications.dat", FileMode.OpenOrCreate);
		BinaryFormatter bf = new BinaryFormatter();
		bf.Serialize(fileStream, notificationDashboardDictionary);
		fileStream.Close();
	}

	private void DeserializeNotifications()
	{
		BinaryFormatter bf = new BinaryFormatter();
		if (File.Exists(Application.persistentDataPath + "/Notifications.dat"))
		{
			FileStream fileStream = new FileStream(Application.persistentDataPath + "/Notifications.dat", FileMode.Open);
			notificationDashboardDictionary = bf.Deserialize(fileStream) as Dictionary<int, string>;
			fileStream.Close();
		}
		else
		{
			notificationDashboardDictionary = new Dictionary<int, string>();
		}
	}

	private void OnShowAllPressed()
	{
		dashboardViewAllPanel.SetActive(true);
		dashboardViewAllPanel.GetComponent<ViewAllPanel>().SpawnGraphs(data);
	}

	private void OnDashboardCreated(DashboardData d)
	{
		data.Add(d.ID, d);

		LoadAndSave.Serialize(data);

		if (data.Count == 5)
		{
			createButton.gameObject.SetActive(false);
		}

		createPanel.SetActive(false);
	}

	private void OnCancelButtonPressed()
	{
		createPanel.SetActive(false);
	}

	private void OnDashboardRemoved(string id,string p,string v)
	{
		data.Remove(id);
		LoadAndSave.Serialize(data);
		//throw new NotImplementedException();
	}

	private void OnDashboardEdited(string id, string editType, string v)
	{
		//change the appropriate string value
		switch (editType)
		{
			case "Name":
				data[id].ChangeName(v);
				break;
			case "Description":
				data[id].ChangeDescription(v);
				break;
			case "Level One":
				data[id].ChangeLevelOneCriteria(v);
				break;
			case "Level Two":
				data[id].ChangeLevelTwoCriteria(v);
				break;
			case "Level Three":
				data[id].ChangeLevelThreeCriteria(v);
				break;
			case "Level Four":
				data[id].ChangeLevelFourCriteria(v);
				break;
			case "Level Five":
				data[id].ChangeLevelFiveCriteria(v);
				break;
			default:
				break;
		}

		//serialize the data
		LoadAndSave.Serialize(data);

	}

	private void OnDashboardClicked(string id)
	{
		Debug.Log("display the appropriate dashboard");
		dashboardViewPanel.SetActive(false);

		dashboardEditPanel.SetActive(true);
		currentPanel = dashboardEditPanel;

		//set the values
		dashboardEditPanel.GetComponent<DashboardEditPanel>().SetValues(data[id]);

		//throw new NotImplementedException();
	}

	private void OnViewButtonPressed()
	{
		dashboardViewPanel.SetActive(true);

		currentPanel = dashboardViewPanel;

		dashboardViewPanel.GetComponent<DashboardViewPanel>().SpawnDashboards(data);
	}

	private void Update()
	{
			//detect back button / escape button presses
			if (Input.GetKeyDown(KeyCode.Escape))
			{
				//react accordingly
				OnBackButtonPressed();
			}

			if (swipeLeft)
			{
				mainButtonHolderAnim.SetTrigger("SwipeLeft");
			}

			if (swipeRight)
			{
				mainButtonHolderAnim.SetTrigger("SwipeRight");
			}

			tap = doubleTap = swipeDown = swipeLeft = swipeRight = swipeUp = false;

#if UNITY_EDITOR
			DetectSwipesStandalone();
#else
		DetectSipesMobile();
#endif
		}

	private void OnBackButtonPressed()
	{
		//TODO: implement simple back functionality
		//throw new NotImplementedException();
		if (currentPanel != null)
		{
			currentPanel.SetActive(false);
			currentPanel = null;
		}
		else
		{
			Application.Quit();
		}
	}

	

	private void OnCreateButtonPressed()
	{
		createPanel.SetActive(true);
		currentPanel = createPanel;
	}

	private void OnEditButtonPressed()
	{

		dashboardEditPanel.SetActive(false);

		OnViewButtonPressed();

		if (data.Count == 0)
		{
			editButton.gameObject.SetActive(false);
		}
	}

	private void OnCheckButtonPressed()
	{
		checkPanel.SetActive(true);
		currentPanel = checkPanel;
		SpawnDashboards();
	}

	

	private void SpawnDashboards()
	{
		ClearItems();
		foreach (var item in data)
		{
			//tweek this

			//we want to check the highest value entry in history,
			//then spawn a dashboard for each date until today
			List<DateTime> missingTimes = new List<DateTime>();
			List<DateTime> historyTimes = item.Value.History.Keys.ToList();
			//automatically goes from lowest to highest
			historyTimes.Sort();

			//get the latest date
			DateTime last = historyTimes[historyTimes.Count - 1];
			//get the number of days between today and the latest date
			TimeSpan daysBetween = DateTime.Today - last;
			for (int i = 0; i < daysBetween.Days; i++)
			{
				last = last.AddDays(1);
				missingTimes.Add(last);
			}

			for (int i = 0; i < missingTimes.Count; i++)
			{
				GameObject go = Instantiate(dashboardPrefab, checkContent);
				//set up content
				go.GetComponentInChildren<Text>().text = item.Value.Name + "\n " + missingTimes[i].ToShortDateString(); // we should add the date
				go.GetComponent<DashboardHolder>().SetDD(item.Value);
				//set up listeners
				string id = item.Key;
				go.GetComponentInChildren<Button>().onClick.AddListener(
					delegate { OnDashboardAcceptButtonPressed(id, missingTimes[i]); });

				if (currentItems.ContainsKey(missingTimes[i]))
				{
					currentItems[missingTimes[i]].Add(id, go);
				}
				else
				{
					Dictionary<string, GameObject> dic = new Dictionary<string, GameObject>();
					dic.Add(id, go);
					currentItems.Add(missingTimes[i], dic);
				}
			}

			//DateTime dt = new DateTime();
			if (!item.Value.History.ContainsKey(DateTime.Today))
			{
				GameObject go = Instantiate(dashboardPrefab, checkContent);
				//set up content
				go.GetComponentInChildren<Text>().text = item.Value.Name + "\n " + DateTime.Today.ToShortDateString(); // we should add the date
				go.GetComponent<DashboardHolder>().SetDD(item.Value);
				//set up listeners
				string id = item.Key;
				go.GetComponentInChildren<Button>().onClick.AddListener(
					delegate { OnDashboardAcceptButtonPressed(id, DateTime.Today); });

				if (currentItems.ContainsKey(DateTime.Today))
				{
					currentItems[DateTime.Today].Add(id, go);
				}
				else
				{
					Dictionary<string, GameObject> dic = new Dictionary<string, GameObject>();
					dic.Add(id, go);
					currentItems.Add(DateTime.Today, dic);
				}
			}

		}
	}

	void ClearItems()
	{
		foreach (var date in currentItems.Keys)
		{
			foreach (var go in currentItems[date].Values)
			{
				Destroy(go);
			}
			currentItems[date].Clear();
		}
		currentItems.Clear();
	}

	void OnDashboardAcceptButtonPressed(string id, DateTime dt)
	{
		//Debug.Log(id);
		data[id].AddDate(dt,
			currentItems[dt][id].GetComponent<DashboardHolder>().stage,
			currentItems[dt][id].GetComponent<DashboardHolder>().commentField.text);
		Debug.Log(currentItems[dt][id].GetComponent<DashboardHolder>().stage);
		Destroy(currentItems[dt][id]);

		LoadAndSave.Serialize(data);
	}

	#region Swiping
	[SerializeField]
	float deadZone = 50f;
	[SerializeField]
	float doubleTapDelta = 0.5f;

	bool tap, doubleTap, swipeLeft, swipeRight, swipeUp, swipeDown;
	Vector2 swipeDelta, startTouch;
	float lastTap;
	float sqrDeadzone;

	void DetectSwipesStandalone()
	{
		if (Input.GetMouseButtonDown(0))
		{

			tap = true;
			startTouch = Input.mousePosition;
			doubleTap = Time.time - lastTap < doubleTapDelta;
			//Debug.Log(Time.time - lastTap);
			lastTap = Time.time;
		}
		else if (Input.GetMouseButtonUp(0))
		{
			startTouch = swipeDelta = Vector2.zero;
		}
		swipeDelta = Vector2.zero;

		if (startTouch != Vector2.zero && Input.GetMouseButton(0))
		{
			swipeDelta = new Vector2(Input.mousePosition.x, Input.mousePosition.y) - startTouch;
		}

		if (swipeDelta.sqrMagnitude > sqrDeadzone)
		{
			float x = swipeDelta.x;
			float y = swipeDelta.y;
			if (Mathf.Abs(x) > Mathf.Abs(y))
			{
				if (x < 0)
				{
					swipeLeft = true;
				}
				else
				{
					swipeRight = true;
				}
			}
			else
			{
				if (y < 0)
				{
					swipeDown = true;
				}
				else
				{
					swipeUp = true;
				}
			}
			//we detected a large enough swipe, so now we will reset
			startTouch = swipeDelta = Vector2.zero;
		}

	}

	void DetectSwipesMobile()
	{
		if (Input.touches.Length != 0)
		{
			if (Input.touches[0].phase == TouchPhase.Began)
			{
				tap = true;
				startTouch = Input.touches[0].position;
				doubleTap = Time.time - lastTap < doubleTapDelta;
				//Debug.Log(Time.time - lastTap);
				lastTap = Time.time;
			}
			else if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
			{
				startTouch = swipeDelta = Vector2.zero;
			}
		}
		swipeDelta = Vector2.zero;

		if (startTouch != Vector2.zero && Input.touches.Length != 0)
		{
			swipeDelta = Input.touches[0].position - startTouch;
		}

		if (swipeDelta.sqrMagnitude > sqrDeadzone)
		{
			float x = swipeDelta.x;
			float y = swipeDelta.y;
			if (Mathf.Abs(x) > Mathf.Abs(y))
			{
				if (x < 0)
				{
					swipeLeft = true;
				}
				else
				{
					swipeRight = true;
				}
			}
			else
			{
				if (y < 0)
				{
					swipeDown = true;
				}
				else
				{
					swipeUp = true;
				}
			}
			//we detected a large enough swipe, so now we will reset
			startTouch = swipeDelta = Vector2.zero;
		}

	}

	#endregion
}
