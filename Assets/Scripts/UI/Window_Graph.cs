﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Window_Graph : MonoBehaviour
{
	[SerializeField]
	private Sprite dotSprite;
	[SerializeField]
	private Sprite triSprite;
	private RectTransform graphContainer;
	private RectTransform labelTemplateX;
	private RectTransform labelTemplateY;
	private RectTransform dashTemplateX;
	private RectTransform dashTemplateY;
	private GameObject tooltipGameObject;

	private List<GameObject> graphObjects = new List<GameObject>();
	//public List<DateTime> dates = new List<DateTime>();

	private void Awake()
	{
		graphContainer = transform.Find("graphContainer").GetComponent<RectTransform>();

		labelTemplateX = graphContainer.Find("labelTemplateX").GetComponent<RectTransform>();
		labelTemplateY = graphContainer.Find("labelTemplateY").GetComponent<RectTransform>();

		dashTemplateX = graphContainer.Find("dashTemplateY").GetComponent<RectTransform>();
		dashTemplateY = graphContainer.Find("dashTemplateX").GetComponent<RectTransform>();

		tooltipGameObject = graphContainer.Find("tooltip").gameObject;

		//for testing
		List<int> valueList = new List<int>() { 1, 3, 5, 2, 4, 3, 1, 5, 5, 4, 3 };
		List<DateTime> dates = new List<DateTime>() {new DateTime(2020,3,19), new DateTime(2020, 3, 20),
		new DateTime(2020,3,21), new DateTime(2020,3,22), new DateTime(2020,3,23),
			new DateTime(2020,3,24),new DateTime(2020,3,25),new DateTime(2020,3,26),new DateTime(2020,3,27),
		new DateTime(2020,3,28),new DateTime(2020,3,29)};

		List<string> comments = new List<string>() {"bad", "better", "OK", "good", "bad", "horrible",
		"bad", "bad", "wonderful", "great","OK"};

		SetData(valueList, dates, comments);

		//Texture2D tex = new Texture2D((int)graphContainer.sizeDelta.x, (int)graphContainer.sizeDelta.y);
		//float xSize = graphContainer.sizeDelta.x / (11 + 1);
		//int xIndex = 0;
		//float yMin = 0f;
		//float yMax = 6f;

		//float yPos = ((valueList[0] - yMin) / (yMax - yMin)) * graphContainer.sizeDelta.y;
		//float xPos = xSize + xIndex * xSize;
		//DrawLine(ref tex, new Vector2(0, (int)yPos), new Vector2((int)xPos, (int)yPos), Color.black);
		//for (int i =  0; i < valueList.Count; i++)
		//{
		//	xPos = xSize + xIndex * xSize;
		//	yPos = ((valueList[i] - yMin) / (yMax - yMin)) * graphContainer.sizeDelta.y;

		//	//Debug.Log(xPos);
		//	//Debug.Log(yPos);
		//	tex.SetPixel((int)xPos, (int)yPos, Color.black);
		//	if (i < valueList.Count-1)
		//	{
		//		float xPos2 = xSize + (xIndex + 1) * xSize;
		//		float yPos2 = ((valueList[i + 1] - yMin) / (yMax - yMin)) * graphContainer.sizeDelta.y;
		//		DrawLine(ref tex, new Vector2((int)xPos, (int)yPos), new Vector2((int)xPos2, (int)yPos2), Color.black);
		//	}
		//	xIndex++;
		//}

		//yPos = ((valueList[valueList.Count-1] - yMin) / (yMax - yMin)) * graphContainer.sizeDelta.y;
		//xPos = xSize + (xIndex-1) * xSize;
		////Debug.Log(xPos);
		//DrawLine(ref tex, new Vector2((int)xPos, (int)yPos), new Vector2((int)tex.width-1, (int)yPos), Color.black);
		////FloodFill(ref tex, Vector2.zero, Color.white, Color.black);

		//tex.Apply();
		//Sprite sp = Sprite.Create(tex, new Rect(graphContainer.rect), new Vector2(0.5f, 0.5f));
		//GameObject go = new GameObject("bar", typeof(Image));
		//go.GetComponent<Image>().sprite = sp;
		//go.transform.SetParent(graphContainer, false);

		
	}

	public void SetData(List<int> success, List<DateTime> dates, List<string> comments)
	{
		//for (int i = 0; i < success.Count; i++)
		//{
		//	Debug.Log(success[i]);
		//}
		//IGraphVisual igv = new LineGraphVisual(graphContainer, dotSprite, Color.green, new Color(1, 1, 1, 0.5f));
		IGraphVisual igv = new CenteredBarChartVisual(graphContainer,.95f);
		//IGraphVisual igv = new FilledLineChartVisual(graphContainer, 1f, triSprite);

		Func<int, string> xLabel = delegate (int i) { return dates[i].ToShortDateString(); };

		//make a y-label func to show the comment
		Func<int, string> yLabel = delegate (int s) { return comments[s]; };

		Func<int, Color> colorFunc = delegate (int i) 
		{
			switch (i)
			{
				case 1:
					return Color.red;
				case 2:
					return new Color(1f, 0.64f, 0f);
				case 3:
					return Color.yellow;
				case 4:
					return Color.blue;
				case 5:
					return Color.green;
			}
			return new Color();
		};

		ShowGraph(success, igv, -1, xLabel, yLabel, colorFunc);
	}

	public void ShowTooltip(string text, Vector2 pos)
	{
		tooltipGameObject.SetActive(true);
		Text t = tooltipGameObject.transform.Find("text").GetComponent<Text>();
		t.text = text;
		Vector2 backgroundSize = new Vector2( t.preferredWidth + 8f, t.preferredHeight + 8f);
		tooltipGameObject.transform.Find("background").GetComponent<RectTransform>().sizeDelta = backgroundSize;
		tooltipGameObject.GetComponent<RectTransform>().anchoredPosition = pos;
		tooltipGameObject.transform.SetAsLastSibling();
	}

	public void HideTooltip(string text, Vector2 pos)
	{
		tooltipGameObject.SetActive(false);
	}

	private void ShowGraph(List<int> valueList, IGraphVisual igv, int maxValuesShown = -1,
		Func<int,string> getAxisLabelX = null, Func<int, string> getAxisLabelY = null, Func<int, Color> getBarColor = null)
	{
		float graphHeight = graphContainer.sizeDelta.y;
		float graphWidth = graphContainer.sizeDelta.x;
		if (maxValuesShown <= 0)
		{
			maxValuesShown = valueList.Count;
		}

		float yMax = valueList[0];
		float yMin = valueList[0];

		//making the y axis dynamic
		for (int i = Mathf.Max(valueList.Count - maxValuesShown,0); i < valueList.Count; i++)
		{
			int value = valueList[i];
			if (value > yMax)
			{
				yMax = value;
			}

			if (value < yMin)
			{
				yMin = value;
			}
		}

		yMin = 0f;
		float yDiff = yMax - yMin;
		if (yDiff <= 0f)
		{
			yDiff = 5f;
		}

		yMax = yMax + (yDiff * .2f);
		//yMin = yMin - (yDiff * .2f);

		//forces graph to start at 0 on the y axis
		//yMin = 0f;

		float xSize = graphWidth / (maxValuesShown + 1);
		

		//I do not understand this code
		if (getAxisLabelX == null)
		{
			getAxisLabelX = delegate (int _i) { return _i.ToString(); };
		}

		if (getAxisLabelY == null)
		{
			getAxisLabelY = delegate (int _i) { return Mathf.RoundToInt(_i).ToString(); };
		}

		if (getBarColor == null)
		{
			getBarColor = delegate (int i)
			{
				return Color.white;
			};
		}

		foreach (var go in graphObjects)
		{
			Destroy(go);
		}
		graphObjects.Clear();

		int xIndex = 0;
		for (int i = Mathf.Max(valueList.Count - maxValuesShown, 0); i < valueList.Count; i++)
		{
			//Debug.Log(valueList[i]);
			float xPos = xSize + xIndex * xSize;
			float yPos = ((valueList[i] - yMin) / (yMax-yMin)) * graphHeight;

			//Debug.Log(i);
			string tooltip = getAxisLabelY(i);
			graphObjects.AddRange(igv.AddGraphVisual(new Vector2(xPos, yPos), xSize, tooltip,
				getBarColor(valueList[i])));

			RectTransform labelX = Instantiate(labelTemplateX);
			graphObjects.Add(labelX.gameObject);
			labelX.SetParent(graphContainer,false);
			labelX.gameObject.SetActive(true);
			labelX.anchoredPosition = new Vector2(xPos, -20f);
			labelX.GetComponent<Text>().text = getAxisLabelX(i);

			RectTransform dashX = Instantiate(dashTemplateX);
			graphObjects.Add(dashX.gameObject);
			dashX.SetParent(graphContainer, false);
			dashX.gameObject.SetActive(true);
			dashX.anchoredPosition = new Vector2(xPos, 0f);

			xIndex++;
		}

		int sepCount = 10;
		for (int i = 0; i <= sepCount; i++)
		{
			RectTransform labelY = Instantiate(labelTemplateY);
			graphObjects.Add(labelY.gameObject);
			labelY.SetParent(graphContainer,false);
			labelY.gameObject.SetActive(true);
			float normalizedValue = i * 1f / sepCount;
			labelY.anchoredPosition = new Vector2(-7f, normalizedValue * graphHeight);
			//labelY.GetComponent<Text>().text = getAxisLabelY(yMin + (normalizedValue * (yMax - yMin)));

			RectTransform dashY = Instantiate(dashTemplateY);
			graphObjects.Add(dashY.gameObject);
			dashY.SetParent(graphContainer, false);
			dashY.gameObject.SetActive(true);
			normalizedValue = i * 1f / sepCount;
			dashY.anchoredPosition = new Vector2(0f, normalizedValue * graphHeight);
		}
	}

	private void ShowGraph(List<float> valueList, IGraphVisual igv, int maxValuesShown = -1,
		Func<int, string> getAxisLabelX = null, Func<float, string> getAxisLabelY = null)
	{
		if (valueList.Count == 0)
		{
			return;
		}

		//Debug.Log(maxValuesShown);
		float graphHeight = graphContainer.sizeDelta.y;
		float graphWidth = graphContainer.sizeDelta.x;

		if (maxValuesShown <= 0)
		{
			maxValuesShown = valueList.Count;
		}

		//Debug.Log(maxValuesShown);

		float yMax = 101f; //= valueList[0];
		float yMin = 0f; //= valueList[0];

		//making the y axis dynamic
		//for (int i = Mathf.Max(valueList.Count - maxValuesShown, 0); i < valueList.Count; i++)
		//{
		//	float value = valueList[i];
		//	if (value > yMax)
		//	{
		//		yMax = value;
		//	}

		//	if (value < yMin)
		//	{
		//		yMin = value;
		//	}
		//}

		//yMin = 0f;
		float yDiff = yMax - yMin;
		if (yDiff <= 0f)
		{
			yDiff = 5f;
		}

		//yMax = yMax + (yDiff * .2f);
		//yMin = yMin - (yDiff * .2f);

		//forces graph to start at 0 on the y axis
		//yMin = 0f;

		float xSize = graphWidth / (maxValuesShown + 1);


		//I do not understand this code
		if (getAxisLabelX == null)
		{
			getAxisLabelX = delegate (int _i) { return _i.ToString(); };
		}

		if (getAxisLabelY == null)
		{
			getAxisLabelY = delegate (float _i) { return Mathf.RoundToInt(_i).ToString(); };
		}

		//if (getBarColor == null)
		//{
		//	getBarColor = delegate (int i)
		//	{
		//		return Color.white;
		//	};
		//}

		foreach (var go in graphObjects)
		{
			Destroy(go);
		}
		graphObjects.Clear();

		int xIndex = 0;
		for (int i = Mathf.Max(valueList.Count - maxValuesShown, 0); i < valueList.Count; i++)
		{
			float xPos = xSize + xIndex * xSize;
			float yPos = ((valueList[i] - yMin) / (yMax - yMin)) * graphHeight;

			string tooltip = getAxisLabelY(valueList[i]);
			graphObjects.AddRange(igv.AddGraphVisual(new Vector2(xPos, yPos), xSize, tooltip, new Color()));

			RectTransform labelX = Instantiate(labelTemplateX);
			graphObjects.Add(labelX.gameObject);
			labelX.SetParent(graphContainer, false);
			labelX.gameObject.SetActive(true);
			labelX.anchoredPosition = new Vector2(xPos, -20f);
			labelX.GetComponent<Text>().text = getAxisLabelX(i);

			RectTransform dashX = Instantiate(dashTemplateX);
			graphObjects.Add(dashX.gameObject);
			dashX.SetParent(graphContainer, false);
			dashX.gameObject.SetActive(true);
			dashX.anchoredPosition = new Vector2(xPos, 0f);

			xIndex++;
		}

		int sepCount = 10;
		for (int i = 0; i <= sepCount; i++)
		{
			RectTransform labelY = Instantiate(labelTemplateY);
			graphObjects.Add(labelY.gameObject);
			labelY.SetParent(graphContainer, false);
			labelY.gameObject.SetActive(true);
			float normalizedValue = i * 1f / sepCount;
			labelY.anchoredPosition = new Vector2(-7f, normalizedValue * graphHeight);
			labelY.GetComponent<Text>().text = getAxisLabelY(normalizedValue * 100);//yMin + (normalizedValue * (yMax - yMin)));

			RectTransform dashY = Instantiate(dashTemplateY);
			graphObjects.Add(dashY.gameObject);
			dashY.SetParent(graphContainer, false);
			dashY.gameObject.SetActive(true);
			normalizedValue = i * 1f / sepCount;
			dashY.anchoredPosition = new Vector2(0f, normalizedValue * graphHeight);
		}
	}

	private interface IGraphVisual
	{
		List<GameObject> AddGraphVisual(Vector2 graphPos, float graphWidth, string tooltip, Color c);
	}

	private class BarChartVisual : IGraphVisual
	{
		private RectTransform graphContainer;
		private Color barColor;
		private float barWidthMultiplier;

		public BarChartVisual(RectTransform gc, Color bc, float barWM)
		{
			graphContainer = gc;
			barColor = bc;
			barWidthMultiplier = barWM;
		}

		public List<GameObject> AddGraphVisual(Vector2 graphPos, float graphWidth, string tooltip, Color c)
		{
			GameObject go = CreateBar(graphPos, graphWidth);
			Tooltip tp = go.AddComponent<Tooltip>();
			tp.message = tooltip;
			tp.PointerEntered += graphContainer.GetComponentInParent<Window_Graph>().ShowTooltip;
			tp.PointerExited += graphContainer.GetComponentInParent<Window_Graph>().HideTooltip;
			return new List<GameObject>() { go };
		}

		private GameObject CreateBar(Vector2 pos, float barWidth)
		{
			GameObject gameObject = new GameObject("bar", typeof(Image));
			gameObject.AddComponent<Outline>().effectColor = Color.black;
			gameObject.GetComponent<Outline>().effectDistance = new Vector2(3, -4);
			gameObject.transform.SetParent(graphContainer, false);
			RectTransform rect = gameObject.GetComponent<RectTransform>();
			gameObject.GetComponent<Image>().color = barColor;
			rect.anchoredPosition = new Vector2(pos.x, 0f);
			rect.sizeDelta = new Vector2(barWidth * barWidthMultiplier, pos.y);
			rect.anchorMin = new Vector2(0, 0);
			rect.anchorMax = new Vector2(0, 0);
			rect.pivot = new Vector2(0.5f, 0f);
			return gameObject;
		}
	}

	#region Centered Bar Chart

	private class CenteredBarChartVisual : IGraphVisual
	{
		private RectTransform graphContainer;
		private float barWidthMultiplier;

		public CenteredBarChartVisual(RectTransform gc, float barWM)
		{
			graphContainer = gc;
			barWidthMultiplier = barWM;
		}

		public List<GameObject> AddGraphVisual(Vector2 graphPos, float graphWidth, string tooltip, Color c)
		{
			GameObject go = CreateBar(graphPos, graphWidth, c);
			Tooltip tp = go.AddComponent<Tooltip>();
			tp.message = tooltip;
			tp.PointerEntered += graphContainer.GetComponentInParent<Window_Graph>().ShowTooltip;
			tp.PointerExited += graphContainer.GetComponentInParent<Window_Graph>().HideTooltip;
			return new List<GameObject>() { go };
		}

		private GameObject CreateBar(Vector2 pos, float barWidth, Color c)
		{
			GameObject gameObject = new GameObject("bar", typeof(Image));
			gameObject.transform.SetParent(graphContainer, false);
			RectTransform rect = gameObject.GetComponent<RectTransform>();
			gameObject.GetComponent<Image>().color = c;
			rect.anchoredPosition = new Vector2(pos.x, graphContainer.sizeDelta.y/2);
			rect.sizeDelta = new Vector2(barWidth * barWidthMultiplier, pos.y);
			rect.anchorMin = new Vector2(0, 0);
			rect.anchorMax = new Vector2(0, 0);
			rect.pivot = new Vector2(0.5f, 0.5f);
			return gameObject;
		}
	}

#endregion

	#region Filled Line Chart

	private class FilledLineChartVisual : IGraphVisual
	{
		private RectTransform graphContainer;
		private float barWidthMultiplier;
		private Sprite triSprite;

		public FilledLineChartVisual(RectTransform gc, float barWM, Sprite tri)
		{
			graphContainer = gc;
			barWidthMultiplier = barWM;
			triSprite = tri;
		}


		public List<GameObject> AddGraphVisual(Vector2 graphPos, float graphWidth, string tooltip, Color c)
		{
			List<GameObject> gos = CreateBar(graphPos, graphWidth, c);
			for (int i = 0; i < gos.Count; i++)
			{
				Tooltip tp = gos[i].AddComponent<Tooltip>();
				tp.message = tooltip;
				tp.PointerEntered += graphContainer.GetComponentInParent<Window_Graph>().ShowTooltip;
				tp.PointerExited += graphContainer.GetComponentInParent<Window_Graph>().HideTooltip;
			}
			
			return gos;
		}

		private List<GameObject> CreateBar(Vector2 pos, float barWidth, Color c)
		{
			GameObject gameObject = new GameObject("tri", typeof(Image));
			//gameObject.transform.SetParent(graphContainer, false);
			//RectTransform rect = gameObject.GetComponent<RectTransform>();
			//gameObject.GetComponent<Image>().sprite = triSprite;
			//gameObject.GetComponent<Image>().color = c;
			
			//rect.sizeDelta = new Vector2(barWidth * barWidthMultiplier, pos.y/2);
			//rect.anchoredPosition = new Vector2(pos.x, graphContainer.sizeDelta.y / 2 + rect.sizeDelta.y / 2);
			//rect.anchorMin = new Vector2(0, 0);
			//rect.anchorMax = new Vector2(0, 0);
			//rect.pivot = new Vector2(0.5f, 0.5f);

			GameObject gameObject2 = new GameObject("tri", typeof(Image));
			//gameObject2.transform.SetParent(graphContainer, false);
			//rect = gameObject2.GetComponent<RectTransform>();
			//gameObject2.GetComponent<Image>().sprite = triSprite;
			//gameObject2.GetComponent<Image>().color = c;
			
			//rect.sizeDelta = new Vector2(barWidth * barWidthMultiplier, pos.y/2);
			//rect.anchoredPosition = new Vector2(pos.x, graphContainer.sizeDelta.y / 2 - rect.sizeDelta.y / 2);
			//rect.anchorMin = new Vector2(0, 0);
			//rect.anchorMax = new Vector2(0, 0);
			//rect.pivot = new Vector2(0.5f, 0.5f);
			//gameObject2.transform.localEulerAngles = new Vector3(0, 0, 180);
			return new List<GameObject>() { gameObject, gameObject2 };
		}
	}

#endregion

	#region Line Graph

	private class LineGraphVisual : IGraphVisual
	{
		private RectTransform graphContainer;
		private Sprite dotSprite;
		private Color lineColor;
		private Color dotColor;

		private GameObject previousDot;

		public LineGraphVisual(RectTransform gc, Sprite dot, Color lc, Color dc)
		{
			graphContainer = gc;
			dotSprite = dot;
			lineColor = lc;
			dotColor = dc;
			previousDot = null;
		}

		public List<GameObject> AddGraphVisual(Vector2 graphPos, float graphWidth, string tooltip, Color c)
		{
			GameObject dotGameObject = CreateDot(graphPos);
			Tooltip tp = dotGameObject.AddComponent<Tooltip>();
			tp.message = tooltip;
			tp.PointerEntered += graphContainer.GetComponentInParent<Window_Graph>().ShowTooltip;
			tp.PointerExited += graphContainer.GetComponentInParent<Window_Graph>().HideTooltip;
			//graphObjects.Add(dotGameObject);
			GameObject connection = null;
			if (previousDot != null)
			{
				connection = CreateDotConnection(previousDot.GetComponent<RectTransform>().anchoredPosition,
					dotGameObject.GetComponent<RectTransform>().anchoredPosition);

			}

			previousDot = dotGameObject;
			return new List<GameObject>() { dotGameObject, connection };
		}

		

		private GameObject CreateDot(Vector2 anchoredPos)
		{
			GameObject gameObject = new GameObject("dot", typeof(Image));
			gameObject.transform.SetParent(graphContainer, false);
			gameObject.GetComponent<Image>().sprite = dotSprite;
			gameObject.GetComponent<Image>().color = dotColor;
			RectTransform rect = gameObject.GetComponent<RectTransform>();
			rect.anchoredPosition = anchoredPos;
			rect.sizeDelta = new Vector2(11, 11);
			rect.anchorMin = new Vector2(0, 0);
			rect.anchorMax = new Vector2(0, 0);
			return gameObject;
		}

		private GameObject CreateDotConnection(Vector2 posA, Vector2 posB)
		{
			GameObject gameObject = new GameObject("dotConnection", typeof(Image));
			gameObject.transform.SetParent(graphContainer, false);
			gameObject.GetComponent<Image>().raycastTarget = false;
			gameObject.GetComponent<Image>().color = lineColor;
			RectTransform rect = gameObject.GetComponent<RectTransform>();
			Vector2 dir = (posB - posA).normalized;
			float distance = Vector2.Distance(posA, posB);
			rect.anchoredPosition = posA + dir * distance * 0.5f;
			rect.sizeDelta = new Vector2(distance, 3);
			rect.anchorMin = new Vector2(0, 0);
			rect.anchorMax = new Vector2(0, 0);

			rect.localEulerAngles = new Vector3(0, 0, Mathf.Rad2Deg * Mathf.Atan2(dir.y, dir.x));

			return gameObject;
		}
	}

	static void DrawLine(ref Texture2D tex, Vector2 start, Vector2 end, Color c)
	{
		int dx = Mathf.FloorToInt(Mathf.Abs(end.x - start.x));
		int sx = start.x < end.x ? 1 : -1;

		int dy = Mathf.FloorToInt(Mathf.Abs(end.y - start.y));
		int sy = start.y < end.y ? 1 : -1;
		int err = (dx > dy ? dx : -dy) / 2;
		int e2;
		for (; ; )
		{
			tex.SetPixel((int)start.x, (int)start.y, c);
			if (start.x == end.x && start.y == end.y)
			{
				break;
			}

			e2 = err;
			if (e2 > -dx)
			{
				err -= dy;
				start.x += sx;
			}

			if (e2 < dy)
			{
				err += dx;
				start.y += sy;
			}
		}

		tex.Apply();
	}

	#endregion

	//static void FloodFill(ref Texture2D tex, Vector2 start, Color targetColor, Color replacementColor)
	//{
	//	targetColor = tex.GetPixel((int)start.x, (int)start.y);
	//	if (targetColor.Equals(replacementColor))
	//	{
	//		return;
	//	}

	//	Stack<Vector2> pixels = new Stack<Vector2>();

	//	pixels.Push(start);
	//	while (pixels.Count != 0)
	//	{
	//		Vector2 temp = pixels.Pop();

	//		int y1 = (int)temp.y;
	//		while (y1 >=0 && tex.GetPixel((int)temp.x,y1) == targetColor)
	//		{
	//			y1--;
	//		}

	//		y1++;
	//		bool spanLeft = false;
	//		bool spanRight = false;
	//		while (y1 < tex.height && tex.GetPixel((int)temp.x,y1) == targetColor)
	//		{
	//			tex.SetPixel((int)temp.x, y1, replacementColor);
	//			if (!spanLeft && temp.x > 0 && tex.GetPixel((int)temp.x-1, y1) == targetColor)
	//			{
	//				pixels.Push(new Vector2(temp.x - 1, y1));
	//				spanLeft = true;
	//			}
	//			else if (spanLeft && (temp.x -1 >= 0 && tex.GetPixel((int)temp.x - 1, y1) != targetColor))
	//			{
	//				spanLeft = false;
	//			}
	//			else if (!spanRight && temp.x < tex.width && tex.GetPixel((int)temp.x + 1, y1) == targetColor)
	//			{
	//				pixels.Push(new Vector2(temp.x + 1, y1));
	//				spanRight = true;
	//			}
	//			else if (spanRight && temp.x < tex.width - 1 && tex.GetPixel((int)temp.x + 1, y1) != targetColor)
	//			{
	//				spanRight = false;
	//			}
	//			y1++;
	//		}
	//	}

	//	tex.Apply();
	//}
}
