﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MonthPanel : MonoBehaviour
{
	public Text monthInfo;
	public GameObject daysHolder;
	public GameObject habitPrefab;

	public Button previousMonthButton, nextMonthButton;

	Text[] daysOfMonth;// = new List<Text>;
	Dictionary<string, List<GameObject>> habitObjects = new Dictionary<string, List<GameObject>>();
	Dictionary<GameObject, List<GameObject>> weekdayHabits = new Dictionary<GameObject, List<GameObject>>();
	Dictionary<int, GameObject> daysOfMonthDictionary = new Dictionary<int, GameObject>();

	public delegate void MonthDayHandler(int year, int month, int day);
	public event MonthDayHandler MonthDayClicked;

	private void Awake()
	{
		daysOfMonth = daysHolder.GetComponentsInChildren<Text>();
		for (int i = 0; i < daysHolder.transform.childCount; i++)
		{
			weekdayHabits.Add(daysHolder.transform.GetChild(i).GetChild(1).gameObject, new List<GameObject>());
		}
	}

	public void InitializeMonth(int year, int month)
	{
		monthInfo.text = ((Months)month).ToString() + ", " + year.ToString();
		DateTime test = new DateTime(year, month, 1);

		daysOfMonthDictionary.Clear();

		int startingPos = (test.DayOfWeek == DayOfWeek.Sunday ? (int)test.DayOfWeek + 6 : (int)test.DayOfWeek - 1);
		//deactivate the unneeded days
		for (int i = 0; i < startingPos; i++)
		{
			daysOfMonth[i].gameObject.SetActive(false);
			daysOfMonth[i].GetComponentInParent<Button>().onClick.RemoveAllListeners();
		}

		int counter = 1;
		for (int i = startingPos; i < DateTime.DaysInMonth(year,month) + startingPos; i++)
		{
			//activate the object
			daysOfMonth[i].gameObject.SetActive(true);

			//clear the listeners
			daysOfMonth[i].GetComponentInParent<Button>().onClick.RemoveAllListeners();

			//set text to the correct number
			daysOfMonth[i].text = counter.ToString();

			//weird, but prevents out of range errors
			int day = counter;
			int y = year;
			int m = month;

			daysOfMonth[i].GetComponentInParent<Button>().onClick.AddListener(delegate { OnMonthDayClicked(y,m,day); }) ;
			daysOfMonthDictionary.Add(counter, daysHolder.transform.GetChild(i).GetChild(1).gameObject);
			counter++;
		}

		//deactivate the extra days at the end
		for (int i = DateTime.DaysInMonth(year, month) + startingPos; i < daysOfMonth.Length ; i++)
		{
			daysOfMonth[i].gameObject.SetActive(false);
			daysOfMonth[i].GetComponentInParent<Button>().onClick.RemoveAllListeners();
		}
		
	}

	protected void OnMonthDayClicked(int year, int month, int day)
	{
		if (MonthDayClicked != null)
		{
			MonthDayClicked(year, month, day);
		}
	}

	//public void SpawnHabits(Dictionary<Habit, List<IPeriod>> habits)
	//{
	//	ClearList();
	//	foreach (var habit in habits)
	//	{
	//		for (int i = 0; i < habit.Value.Count; i++)
	//		{
	//			//get the day of month and convert it to the proper range
	//			int index = habit.Value[i].StartTime.Day;
	//			//Debug.Log(daysOfMonthDictionary[index]);

	//			if (weekdayHabits[daysOfMonthDictionary[index]].Count < 4)
	//			{
	//				//we have a small enough number of habits to display
	//				//Debug.Log(daysOfMonthDictionary[index].name);
	//				GameObject habitObject = Instantiate(habitPrefab, daysOfMonthDictionary[index].transform);
	//				AddHabitObject(daysOfMonthDictionary[index], habitObject, habit.Key);
	//			}
	//			else
	//			{
	//				//we should show 
	//			}



	//		}
	//	}
	//}

	//void AddHabitObject(GameObject holder, GameObject habit, Habit hab)
	//{
	//	weekdayHabits[holder].Add(habit);
	//	if (!habitObjects.ContainsKey(hab.HabitID))
	//	{
	//		habitObjects.Add(hab.HabitID, new List<GameObject>() { habit });
	//	}
	//	else
	//	{
	//		habitObjects[hab.HabitID].Add(habit);
	//	}
	//	habit.GetComponent<Image>().color = hab.HabitColor;
	//}

	public void ClearList()
	{
		List<string> keys = habitObjects.Keys.ToList();

		for (int i = 0; i < keys.Count; i++)
		{
			//avoid out of range errors
			for (int x = habitObjects[keys[i]].Count - 1; x > -1; x--)
			{
				Destroy(habitObjects[keys[i]][x]);
			}
		}

		keys.Clear();
		habitObjects.Clear();
		foreach (var day in weekdayHabits)
		{
			day.Value.Clear();
		}
	}

}
