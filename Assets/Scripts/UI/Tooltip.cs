﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Tooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public delegate void PointerHandler(string text, Vector2 pos);
	public event PointerHandler PointerEntered;
	public event PointerHandler PointerExited;

	public string message;

	public void OnPointerEnter(PointerEventData eventData)
	{
		if (PointerEntered != null)
		{
			Vector2 anchor = gameObject.GetComponent<RectTransform>().anchoredPosition;
			//anchor.y += gameObject.GetComponent<RectTransform>().sizeDelta.y;
			PointerEntered(message, anchor);
		}
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		if (PointerExited != null)
		{
			PointerExited("", Vector2.zero);
		}
	}

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
