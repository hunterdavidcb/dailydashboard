﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class DayPanel : MonoBehaviour
{
	public Text dayInfo;
	public GameObject hoursHolder;
	public GameObject dayHabitPrefab;
	Dictionary<string, List<GameObject>> habitObjects = new Dictionary<string, List<GameObject>>();
	Dictionary<int, GameObject> hoursDictionary = new Dictionary<int, GameObject>();

	public Button previousDayButton, nextDayButton;

	public delegate void HabitClickHandler(string id);
	public event HabitClickHandler HabitClicked;

	private void Awake()
	{
		for (int i = 0; i < hoursHolder.transform.childCount; i++)
		{
			hoursDictionary.Add(i, hoursHolder.transform.GetChild(i).gameObject);
		}
	}

	//public void SpawnHabits(Dictionary<Habit, List<IPeriod>> habits)
	//{
	//	ClearList();
	//	//Debug.Log("here");
	//	//Debug.Log(habits.Count);
	//	foreach (var hab in habits)
	//	{
	//		string x = hab.Key.HabitID;
	//		var pa = new RecurrencePattern(hab.Key.RepititionRules[0]);
	//		//Debug.Log(pa.ToString());
	//		foreach (var period in hab.Value)
	//		{
	//			string time = period.StartTime.Hour.ToString("D2") + ":" + period.StartTime.Minute.ToString("D2");
	//			//string time = pa.ByHour[0].ToString("D2") + ":" + pa.ByMinute[0].ToString("D2");
	//			GameObject habit = Instantiate(dayHabitPrefab, hoursDictionary[period.StartTime.Hour].transform);
	//			//Debug.Log(time);
	//			//Debug.Log(habit.GetComponent<TodayHabit>().question);
	//			habit.GetComponentInChildren<Text>().text = hab.Key.HabitName;
	//			habit.GetComponent<Button>().onClick.AddListener(delegate { HabitClicked(hab.Key.HabitID); });
	//			habit.GetComponent<Image>().color = hab.Key.HabitColor;
	//			//habit.GetComponent<TodayHabit>().message.text = habit.GetComponent<TodayHabit>().question.Replace("***", hab.Key.HabitName + " at " + time);

	//			if (habitObjects.ContainsKey(x))
	//			{
	//				habitObjects[x].Add(habit);
	//			}
	//			else
	//			{
	//				habitObjects.Add(x, new List<GameObject>() { habit });
	//			}


	//		}
	//	}
	//}

	public void ClearList()
	{
		List<string> keys = habitObjects.Keys.ToList();

		for (int i = 0; i < keys.Count; i++)
		{
			//avoid out of range errors
			for (int x = habitObjects[keys[i]].Count - 1; x > -1; x--)
			{
				Destroy(habitObjects[keys[i]][x]);
			}
		}

		keys.Clear();
		habitObjects.Clear();
	}

}
