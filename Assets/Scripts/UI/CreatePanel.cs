﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreatePanel : MonoBehaviour
{
	
	public InputField nameInput;
	public InputField descriptionInput;

	public InputField lOneInput;
	public InputField lTwoInput;
	public InputField lThreeInput;
	public InputField lFourInput;
	public InputField lFiveInput;

	public Button acceptButton;
	public Button cancelButton;

	public Text errorText;

	public delegate void DashboardCreationHandler(DashboardData d);
	public event DashboardCreationHandler DashboardCreated;

	// Start is called before the first frame update
	void Start()
    {
		errorText.gameObject.SetActive(false);

		nameInput.onEndEdit.AddListener(OnNameEdited);
		descriptionInput.onEndEdit.AddListener(OnDescriptionEdited);

		acceptButton.onClick.AddListener(OnAcceptButtonPressed);
	}

    // Update is called once per frame
    void Update()
    {
        
    }

	private void OnDescriptionEdited(string desc)
	{
		if (desc.Length > 0)
		{
			errorText.gameObject.SetActive(false);
		}
		else
		{
			errorText.gameObject.SetActive(true);
			errorText.text = "Please enter a description.";
		}
	}

	private void OnNameEdited(string na)
	{
		if (na.Length > 0)
		{
			errorText.gameObject.SetActive(false);
			descriptionInput.Select();
		}
		else
		{
			errorText.gameObject.SetActive(true);
			errorText.text = "Please enter a name.";
		}
	}

	private void OnAcceptButtonPressed()
	{

		if (nameInput.text.Length == 0)
		{
			errorText.gameObject.SetActive(true);
			errorText.text = "Please enter a name.";
			return;
		}

		if (descriptionInput.text.Length == 0)
		{
			errorText.gameObject.SetActive(true);
			errorText.text = "Please enter a description.";
			return;
		}

		if (lOneInput.text.Length == 0)
		{
			errorText.gameObject.SetActive(true);
			errorText.text = "Please enter the performance criteria for level one.";
			return;
		}

		if (lTwoInput.text.Length == 0)
		{
			errorText.gameObject.SetActive(true);
			errorText.text = "Please enter the performance criteria for level two.";
			return;
		}

		if (lThreeInput.text.Length == 0)
		{
			errorText.gameObject.SetActive(true);
			errorText.text = "Please enter the performance criteria for level three.";
			return;
		}

		if (lFourInput.text.Length == 0)
		{
			errorText.gameObject.SetActive(true);
			errorText.text = "Please enter the performance criteria for level four.";
			return;
		}

		if (lFiveInput.text.Length == 0)
		{
			errorText.gameObject.SetActive(true);
			errorText.text = "Please enter the performance criteria for level five.";
			return;
		}



		DashboardData d = new DashboardData(nameInput.text, descriptionInput.text,
			lOneInput.text, lTwoInput.text, lThreeInput.text, lFourInput.text, lFiveInput.text);

		
		if (DashboardCreated != null)
		{
			DashboardCreated(d);
		}

		OnCancelButtonPressed();

	}

	private void OnCancelButtonPressed()
	{
		nameInput.text = "";
		((Text)nameInput.placeholder).text = "Enter text";

		descriptionInput.text = "";
		((Text)descriptionInput.placeholder).text = "Enter text";

		errorText.gameObject.SetActive(false);
	}
}
