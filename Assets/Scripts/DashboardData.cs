﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DashboardData
{
	string id;
	string name;
	string description;
	string levelOnePerformanceCriteria;
	string levelTwoPerformanceCriteria;
	string levelThreePerformanceCriteria;
	string levelFourPerformanceCriteria;
	string levelFivePerformanceCriteria;
	Dictionary<DateTime, DateEntry> history;
	DateTime start;

	#region Getter Properties

	public string ID
	{
		get { return id; }
	}

	public string Name
	{
		get { return name; }
	}

	public string Description
	{
		get { return description; }
	}

	public string LevelOnePerformanceCriteria
	{
		get { return levelOnePerformanceCriteria; }
	}

	public string LevelTwoPerformanceCriteria
	{
		get { return levelTwoPerformanceCriteria; }
	}

	public string LevelThreePerformanceCriteria
	{
		get { return levelThreePerformanceCriteria; }
	}

	public string LevelFourPerformanceCriteria
	{
		get { return levelFourPerformanceCriteria; }
	}

	public string LevelFivePerformanceCriteria
	{
		get { return levelFivePerformanceCriteria; }
	}

	public Dictionary<DateTime, DateEntry> History
	{
		get { return history; }
	}

	public DateTime Start
	{
		get { return start; }
	}

	#endregion

	#region Constructors

	public DashboardData()
	{
		id = Guid.NewGuid().ToString();
		history = new Dictionary<DateTime, DateEntry>();
		start = DateTime.Today;
	}

	public DashboardData(string n)
	{
		id = Guid.NewGuid().ToString();
		name = n;
		history = new Dictionary<DateTime, DateEntry>();
		start = DateTime.Today;
	}

	public DashboardData(string n, string d)
	{
		id = Guid.NewGuid().ToString();
		name = n;
		description = d;
		history = new Dictionary<DateTime, DateEntry>();
		start = DateTime.Today;
	}

	public DashboardData(string n, string d, string l1, string l2, string l3, string l4, string l5)
	{
		id = Guid.NewGuid().ToString();
		name = n;
		description = d;
		levelOnePerformanceCriteria = l1;
		levelTwoPerformanceCriteria = l2;
		levelThreePerformanceCriteria = l3;
		levelFourPerformanceCriteria = l4;
		levelFivePerformanceCriteria = l5;
		history = new Dictionary<DateTime, DateEntry>();
		start = DateTime.Today;
	}

	public DashboardData(string n, string d, string l1, string l2, string l3, string l4, string l5, DateTime s)
	{
		id = Guid.NewGuid().ToString();
		name = n;
		description = d;
		levelOnePerformanceCriteria = l1;
		levelTwoPerformanceCriteria = l2;
		levelThreePerformanceCriteria = l3;
		levelFourPerformanceCriteria = l4;
		levelFivePerformanceCriteria = l5;
		history = new Dictionary<DateTime, DateEntry>();
		start = s;
	}

	#endregion

	public float SuccessPercentage
	{
		get
		{
			int success = 0;
			int total = history.Count == 0 ? 1 : history.Count;
			foreach (var instance in history)
			{
				success += instance.Value.level;

			}
			return (float)success / 5f / total * 100f;
		}

	}

	public float SuccessPercentAtDate(DateTime until)
	{
		int success = 0;
		int total = 0;
		if (history.Count == 0)
		{
			return 0f;
		}

		foreach (var instance in history)
		{
			if (instance.Key <= until)
			{

				success += instance.Value.level;
				total++;
			}
			else
			{
				break;
			}

		}
		return (float)success / 5f / total * 100f;
	}

	#region Public Member Functions

	public void AddDate(DateTime dt, int rating, string s)
	{
		if (!history.ContainsKey(dt))
		{
			history.Add(dt, new DateEntry(rating, s));
		}
	}

	public void AddDate(DateTime dt, DateEntry de)
	{
		if (!history.ContainsKey(dt))
		{
			history.Add(dt, de);
		}
	}

	public void ChangeName(string n)
	{
		name = n;
	}

	public void ChangeDescription(string d)
	{
		description = d;
	}

	public void ChangeLevelOneCriteria(string o)
	{
		levelOnePerformanceCriteria = o;
	}

	public void ChangeLevelTwoCriteria(string o)
	{
		levelTwoPerformanceCriteria = o;
	}

	public void ChangeLevelThreeCriteria(string o)
	{
		levelThreePerformanceCriteria = o;
	}

	public void ChangeLevelFourCriteria(string o)
	{
		levelFourPerformanceCriteria = o;
	}

	public void ChangeLevelFiveCriteria(string o)
	{
		levelFivePerformanceCriteria = o;
	}

	public void ChangeStartDate(DateTime s)
	{
		start = s;
	}

	#endregion
}

[Serializable]
public struct DateEntry
{
	public int level;
	public string comment;

	public DateEntry(int r, string s)
	{
		level = r;
		comment = s;
	}
}
