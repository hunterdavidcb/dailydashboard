﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class LoadAndSave
{
    public static void Serialize(Dictionary<string,DashboardData> data)
	{
		string path = Application.persistentDataPath + "/data.dat";
		FileStream stream = new FileStream(path, FileMode.OpenOrCreate);
		BinaryFormatter bf = new BinaryFormatter();
		bf.Serialize(stream, data);
		stream.Close();
	}

	public static Dictionary<string,DashboardData> Deserialize()
	{
		Dictionary<string, DashboardData> data = new Dictionary<string, DashboardData>();

		string path = Application.persistentDataPath + "/data.dat";
		if (File.Exists(path))
		{
			FileStream stream = new FileStream(path, FileMode.Open);

			if (stream != null && stream.Length > 0)
			{
				BinaryFormatter bf = new BinaryFormatter();
				data = bf.Deserialize(stream) as Dictionary<string, DashboardData>;
			}

			stream.Close();
		}
		

		return data;
	}
}
